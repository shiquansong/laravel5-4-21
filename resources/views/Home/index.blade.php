<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- 最新版本的 Bootstrap 核心 CSS 文件 -->
    <link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
    <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>

    <script src="{{asset('/js/vue.js')}}"></script>
    <title>Vue.js 教程</title>

</head>
<body>


{{--<div class="container">--}}
    {{--@foreach ($users as $user)--}}
        {{--{{ $user->name }}-- {{ $user->email }}--}}
    {{--@endforeach--}}
{{--</div>--}}

{{--{!! $users->appends(['type'=>11])->links() !!}--}}

{{--{!! $users->perPage() !!}--}}

<div id="app">
    @{{ message }}
</div>

<div id="app-2">
  <span :title="message">
    鼠标悬停几秒钟查看此处动态绑定的提示信息！
  </span>
</div>
<div id="app-3">
    <p v-if="seen">现在你看到我了</p>
</div>
<div id="app-4">
    <ol>
        <li v-for="todo in todos">
           @{{ todo.text }}    @{{ todo.a }}
        </li>
    </ol>
</div>
<div id="app-6">
    <p>@{{ message }}</p>
    <input v-model="message">
</div>
<ol>
    <!-- 创建一个 todo-item 组件的实例 -->
    <todo-item></todo-item>
</ol>

<script type="text/javascript">
    var app = new Vue({
        el: '#app',
        data: {
            message: 'Hello Vue!'
        }
    })

    var app2 = new Vue({
        el: '#app-2',
        data: {
            message: '页面加载于 ' + new Date()
        }
    })

    var app3 = new Vue({
        el: '#app-3',
        data: {
            seen: false
        }
    })
    var app4 = new Vue({
        el: '#app-4',
        data: {
            todos: [
                { text: '学习 JavaScript','a':11 },
                { text: '学习 Vue' },
                { text: '整个牛项目' }
            ]
        }
    })

    var app6 = new Vue({
        el: '#app-6',
        data: {
            message: 'Hello Vue!'
        }
    })

    Vue.component('todo-item', {
        template: '<li>这是个待办项</li>'
    })
</script>


</body>
</html>